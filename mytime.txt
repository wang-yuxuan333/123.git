#include <stdio.h>
#include <time.h>

int main()
{
    time_t start, end;
    int i;
    start = time(NULL);
    printf("start=%ld\n", start);
    
    // Simulate more computation work by increasing the loop iterations
    for (i = 0; i < 1000000000; i++) {
        // Some dummy computation, you can replace this with actual work
        int result = i * i;
    }
    
    end = time(NULL);
    printf("end=%ld time=%ld\n", end, end - start);
    
    return 0;
}
